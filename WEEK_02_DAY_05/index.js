const { regencies,  provinces} = require('./location');



function getCities(provinceName) {
    const cities = []

    let provinceId = 0;

    // find province where province name equal provinceName
    for(let province of provinces) {
        if(province.name == provinceName) {
            provinceId = province.id;
            break;
        }
    }

    // find city where province_id equals provinceId
    for(let regency of regencies) {
        if(regency.province_id == provinceId) {
            cities.push(regency.name);
        }
    }

    return cities;
}

// const nameProvince = "ACEH";
// const cities = getCities(nameProvince);
// console.log(cities)


function findProvince(cityName) {
    
    for(let regency of regencies) {
        if(regency.name == cityName) {
            regencyId = regency.province_id;
           
        }
    }
    
    for(let province of provinces) {
        if(province.id == regencyId) {
            console.log(province.name);
        }
    }
}

// (findProvince("KOTA BATAM"));



function findCityMoreThanNWords(n) {
    let result = []
    for(let regency of regencies) {
        if(regency.name.split(" ").length == n) {
            result.push(regency.name);
        }
    }
    return result;
}

    // console.log(findCityMoreThanNWords(2));


