let romanToInt = function(inputan) {
    let romanToInt = {
      I: 1,
      V: 5,
      X: 10,
      L: 50,
      C: 100,
      D: 500,
      M: 1000
    }

    let total = 0;

    for (let i = 0; i < inputan.length; i++) { // perulangan jika kondiri inputan memenuhi
      let currentInt = romanToInt[inputan.charAt(i)]; // mencari index pertama
      let nextInt = romanToInt[inputan.charAt(i + 1)]; // mencari index setelahnya

      if (nextInt) {
        if (currentInt >= nextInt) { // jika index pertama > index selanjutnya
          total += currentInt; // print values dari index tsb
        } else {
          total += (nextInt - currentInt); // jika index pertama lebih kecil dari index setelahnya
          i++;                              // index selanjutnya dikurang index pertama
        }
      } else {
        total += currentInt; // dijumlahkan jasil dari index
      }
    }

    return total;
};