function kpk(num1,num2) {   

    let result = 0;


        for(result = num2; result%num1 !=0 || result%num2 !=0;result++); 

        return result;
    
             
}

console.log(kpk(5,7));


/* 
    (5,7)

    kpk 5 = 5,10,15,20,25,30,[35],40,45,50
    kpk 7 = 7,14,21,28,[35],42,49,56

    (4,7)
    kpk 4 = 4,8,12,16,20,24,[28]
    kpk 7 = 7,14,21,[28],35

    (10,6)                              result = num1
    kpk 10 = 10,20,[30],40,50,60        num1 = num2
    kpk 6 = 6,12,18,24,[30] .. 60       num2 = result
                                        
    (7,21)
    kpk 7 = 7,14,[21],28
    kpk 21 = [21],22,32,42
*/